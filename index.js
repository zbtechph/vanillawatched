const fetch = require("node-fetch")
const cheerio = require("cheerio")
const notifier = require("node-notifier")
const url = require("openurl")

const dealUrl = "https://www.lowendtalk.com/discussion/168133/crazy-deals-crazy-giveaway-the-hottest-black-friday-thread-40-prizes-by-racknerd#latest";
//const dealUrl = "https://www.lowendtalk.com/discussion/168133/crazy-deals-crazy-giveaway-the-hottest-black-friday-thread-40-prizes-by-racknerd/p29";

const rep = [
	'dustinc'
];

const giveawayKeywords = [
	"RackNerd giveaway!",
	"FREE KVM",
	"Must comment"
];

async function startMonitor(){
	const html = await fetch(dealUrl).catch(err => {
		console.log(err);
	});
	if(await html.ok){
		const $ = cheerio.load(await html.text());
		$("li.ItemComment").each(item => {
			const comment = $("li.ItemComment")[item]; 
			const uname = $(comment).find(".Username").text();
			if(rep.find( repname => repname === uname)){
				const content = $(comment).find(".userContent").text();
				if( ! giveawayKeywords.find( gKeyw => content.search(gKeyw) < 0 ) ){
					url.open(dealUrl);
					notifier.notify({
						title: "Giveaway started!",
						message: `Racknerd has started a giveaway hosted by ${uname}`,
						sound: true,
					});
					clearInterval(intervalObj);
					console.log("Thread will be unwatched!")
				}
			}

		})
	}
}


startMonitor();

console.log(
	"Watching thread for post by \n"
	+ rep.join(", ")
	+ "\nwith keywords \n"
	+ giveawayKeywords.join(", "));

const intervalObj = setInterval(() => {
	startMonitor();
}, 5000);
